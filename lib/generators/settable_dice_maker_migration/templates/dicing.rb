class Dicing < ActiveRecord::Base
  belongs_to :dice
  belongs_to :dicable, polymorphic: true
  accepts_nested_attributes_for :dice, :reject_if => :all_blank
end
