class DiceSet < ActiveRecord::Base
  has_many :dice_settings, dependent: :destroy
end
