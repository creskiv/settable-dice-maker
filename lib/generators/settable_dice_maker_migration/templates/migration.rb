class SettableDiceMakerMigration < ActiveRecord::Migration
  def change
    create_table :dice_sets do |t|
      t.string :title

      t.timestamps null: false
    end

    create_table :dices do |t|
      t.string :title
      t.string :formula

      t.timestamps null: false
    end

    create_table :dice_settings do |t|
      t.references :dice_set
      t.references :dice_settable, polymorphic: true

      t.datetime :created_at
    end
    add_index :dice_settings, :dice_set_id
    add_index :dice_settings, [:dice_settable_id, :dice_settable_type]

    create_table :dicings do |t|
      t.references :dice
      t.references :dicable, polymorphic: true

      t.datetime :created_at
    end

    add_index :dicings, :dice_id
    add_index :dicings, [:dicable_id, :dicable_type]
  end
end
