class DiceSetting < ActiveRecord::Base
  belongs_to :dice_set
  belongs_to :dice_settable, polymorphic: true
  accepts_nested_attributes_for :dice_set, :reject_if => :all_blank
end
