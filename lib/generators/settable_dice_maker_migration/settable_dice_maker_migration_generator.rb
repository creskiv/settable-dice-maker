class SettableDiceMakerMigrationGenerator < Rails::Generators::Base
  include Rails::Generators::Migration
  
  source_root File.expand_path('../templates', __FILE__)
  
  # Complete wtf that this isn't provided elsewhere.
  def self.next_migration_number(dirname)
    if ActiveRecord::Base.timestamped_migrations
      Time.now.utc.strftime("%Y%m%d%H%M%S")
    else
      "%.3d" % (current_migration_number(dirname) + 1)
    end
  end

  def manifest
    migration_template 'migration.rb', 'db/migrate/settable_dice_maker_migration.rb'
    copy_file 'dice.rb', 'app/models/dice.rb'
    copy_file 'dice_set.rb', 'app/models/dice_set.rb'
    copy_file 'dice_setting.rb', 'app/models/dice_setting.rb'
    copy_file 'dicing.rb', 'app/models/dicing.rb'
  end
end
